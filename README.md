Role Name
=========

An Ansible role to install PostgreSQL.

Requirements
------------

No special requirements.

Role Variables
--------------

You can set these variables in defaults/main.yml instead of using prompts:
  - pgsql_server_hostname
  - pgsql_version
  - init_db
  - enable_pgsql
  - use_firewalld

You can also set the pgsql_server_ip variable in the ansible/hosts file, and set the authentication to this user@ip-address.

Dependencies
------------

No special dependancies.

Example Playbook
----------------

    ---
    - name: Add server to install PostgreSQL on
        connection: local
        hosts: 127.0.0.1
        roles:
          - add-hosts
      
        vars_prompt:
          - name: "pgsql_server_ip"
            prompt: "Enter server to install PostgreSQL on"
            private: no
      
    - name: Install and initiate PostgreSQL
        hosts: pgsqlserver
        roles:
          - postgresql
      
        vars_prompt:
          - name: "pgsql_server_hostname"
            prompt: "Enter server's desirable hostname"
            private: no
            default: "no"
      
          - name: "pgsql_version"
            prompt: "Enter desirable PostgreSQL version"
            private: no
            default: "13"
      
          - name: "init_db"
            prompt: "Initiate Database? (yes/no)"
            private: no
            default: "yes"
      
          - name: "enable_pgsql"
            prompt: "Enable PostgreSQL service? (yes/no)"
            private: no
            default: "yes"
      
          - name: "use_firewalld"
            prompt: "Use firewalld?"
            private: no
            default: "yes"


License
-------

BSD

